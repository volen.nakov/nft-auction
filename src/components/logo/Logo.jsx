function Logo({type}) {
    let location = '/images/logo.svg'
    if(type=='muted'){
        location = '/images/logo-muted.svg'
    }
    if(type=='default'){
        location = '/images/logo.svg'
    }
    return (    
        <div class="Logo">
            <img src={location}></img>
        </div>
    )
}
export default Logo;